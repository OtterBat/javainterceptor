**Java Interceptor**

* Allows for the following:
	- Capture data on local network
	- Run anywhere (.JAR portability)
	- Easily view source and target IPv4's (No IPv6 as of yet)
	- More to come in the future!